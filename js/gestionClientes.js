var clientesObtenidos;

function getClientes(){
  //var url="http://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url="http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange  = function()
  {
    if (this.readyState == 4 && this.status == 200) {
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  };

  request.open("GET", url, true);
  request.send();
}

function procesarClientes()
{
    var jsonClientes = JSON.parse(clientesObtenidos);
//    var acumulado = "";
    var tabla = document.getElementById("tablaClientes");

    for (var i = 0; i < jsonClientes.value.length; i++) {
      var newRow = document.createElement("tr");
      var nameColumn = document.createElement("td");
      var cityColumn = document.createElement("td");
      var phoneColumn = document.createElement("td");
      var flagColumn = document.createElement("td");

      var imgFlag = document.createElement("img");
      var texto = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + jsonClientes.value[i].Country + ".png";

      if (jsonClientes.value[i].Country == "UK"){
        var texto = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
      }

      imgFlag.setAttribute("src", texto);
//      imgFlag.setAttribute("width", 100);
//      imgFlag.setAttribute("heigth", 100);
      imgFlag.classList.add("flag");

      nameColumn.innerText = jsonClientes.value[i].ContactName;
      cityColumn.innerText = jsonClientes.value[i].City;
      phoneColumn.innerText = jsonClientes.value[i].Phone;
//      flagColumn.innerText = jsonClientes.value[i].Country;

      newRow.appendChild (nameColumn);
      newRow.appendChild (cityColumn);
      newRow.appendChild (phoneColumn);
      newRow.appendChild (flagColumn);
      flagColumn.appendChild (imgFlag);

      tabla.appendChild(newRow);

//      acumulado = acumulado + " - " + jsonProductos.value[i].ProductName;
    }
//    alert(acumulado);
//    alert(jsonProductos.value[0].ProductName);
}
