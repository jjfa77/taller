var productosObtenidos;

function getProductos(){
  var url="http://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange  = function()
  {
    if (this.readyState == 4 && this.status == 200) {
      console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  };

  request.open("GET", url, true);
  request.send();
}

function procesarProductos()
{
    var jsonProductos = JSON.parse(productosObtenidos);
//    var acumulado = "";
    var tabla = document.getElementById("tablaProductos");

    for (var i = 0; i < jsonProductos.value.length; i++) {
      var newRow = document.createElement("tr");
      var nameColumn = document.createElement("td");
      var priceColumn = document.createElement("td");
      var stockColumn = document.createElement("td");

      nameColumn.innerText = jsonProductos.value[i].ProductName;
      priceColumn.innerText = jsonProductos.value[i].UnitPrice;
      stockColumn.innerText = jsonProductos.value[i].UnitsInStock;

      newRow.appendChild (nameColumn);
      newRow.appendChild (priceColumn);
      newRow.appendChild (stockColumn);

      tabla.appendChild(newRow);

//      acumulado = acumulado + " - " + jsonProductos.value[i].ProductName;
    }
//    alert(acumulado);
//    alert(jsonProductos.value[0].ProductName);
}
